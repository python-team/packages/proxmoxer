proxmoxer (2.2.0-1) sid; urgency=medium

  * New upstream release.

 -- Elena Grandi <valhalla@debian.org>  Fri, 10 Jan 2025 13:33:19 +0100

proxmoxer (2.1.0-1) sid; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.7.0 (no changes needed).

 -- Elena Grandi <valhalla@debian.org>  Wed, 04 Dec 2024 10:42:53 +0100

proxmoxer (2.0.1-2) sid; urgency=medium

  * Team Upload.
  * remove extraneous python3-mock build-dep
  * use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Thu, 28 Mar 2024 00:54:16 +0100

proxmoxer (2.0.1-1) sid; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.6.2 (no changes needed).
  * Add autopkgtests.

 -- Elena Grandi <valhalla@debian.org>  Mon, 08 Jan 2024 11:44:37 +0100

proxmoxer (1.2.0-2) sid; urgency=medium

  * Rebuild for sid

 -- Elena Grandi <valhalla@debian.org>  Thu, 16 Dec 2021 14:03:15 +0100

proxmoxer (1.2.0-1) experimental; urgency=medium

  * Get the releases from github rather than pypi (upstream removed tests
    from the pypi tarball)
  * New upstream release
  * Skip a test that requires an old version of requests.
  * Bump Standards-Version to 4.6.0.1 (no changes needed).

 -- Elena Grandi <valhalla@debian.org>  Thu, 16 Dec 2021 13:26:38 +0100

proxmoxer (1.1.1-1) experimental; urgency=medium
  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Lorenz Schori ]
  * New upstream release
  * Update upstream source repos and website

  [ Elena Grandi ]
  * Bumped compat to 13.
  * Bumped watch file version.
  * Bumped Standards-Version (no changes needed).
  * Updated Homepage.
  * Added explicit Rules-Requires-Root.

 -- Elena Grandi <valhalla@debian.org>  Sat, 17 Apr 2021 08:52:10 +0200

proxmoxer (1.0.3-2) unstable; urgency=medium

  * Team upload.
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

 -- Ondřej Nový <onovy@debian.org>  Tue, 06 Aug 2019 16:47:05 +0200

proxmoxer (1.0.3-1) unstable; urgency=medium

  * New upstream release
  * Updated uploader address
  * Updated standards version to 4.2.1 (no changes needed)
  * Added Upstream MEtadata GAthered with YAml

 -- Elena Grandi <valhalla@debian.org>  Wed, 26 Sep 2018 21:23:25 +0200

proxmoxer (1.0.2-1) unstable; urgency=low

  [ Elena Grandi ]
  * New package: Closes: #883103.
  * A Python 2 version is also provided as this module is used by ansible,
    which is currently still python2 by default.

 -- Elena Grandi <valhalla-d@trueelena.org>  Mon, 26 Feb 2018 21:27:48 +0100
